(function () {
    angular
      .module('king.services.idfa', [])
      .run(loadFunction);
  
    loadFunction.$inject = ['configService'];

    function loadFunction(configService) {
        // Register upper level modules
        try {
          if (configService.services && configService.services.idfa) {
            askpermissionFunction(configService.services.idfa.scope);
          } else {
            throw "The service is not added to the application";
          }
        } catch (error) {
          console.error("Error", error);
        }
    }

    function  askpermissionFunction(){

        if (device.platform === 'iOS' && parseFloat(device.version) >= 14.5) {
            showAppTrackingTransparency();
          } else {
            console.log("This plugin is only for ios");
        }

        function showAppTrackingTransparency() {
            const idfaPlugin = window.cordova.plugins.idfa;
            idfaPlugin.getInfo().then((info) => {
              if (!info.trackingLimited) {
                return info.idfa || info.aaid;
              } else if (
                info.trackingPermission === idfaPlugin.TRACKING_PERMISSION_NOT_DETERMINED
              ) {
                return idfaPlugin.requestPermission().then((result) => {
                  if (result === idfaPlugin.TRACKING_PERMISSION_AUTHORIZED) {
          
                    // Start your tracking plugin here!
          
                    return idfaPlugin.getInfo().then((info) => {
                      return info.idfa || info.aaid;
                    });
                  }
                });
              }
            });
        }
    }
})();