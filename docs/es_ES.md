# Documentación

Este complemento solicita permiso para rastrear la actividad del usuario en otras empresas, aplicaciones y sitios web. Solo se necesita para iOS.

### Configuración:

Este complemento es plug and play, por lo que no es necesaria ninguna configuración previa
