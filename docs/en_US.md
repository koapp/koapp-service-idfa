# Documentation:

This plugin asks permission to track  user's activity across other companies, apps and websites. Only needed for iOS.

### Configuration:

This plugin is plug and play, so no prior configuration is necessary.
